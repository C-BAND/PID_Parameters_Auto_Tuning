function [Zp,sjb_P,Ts_P,JingCha,K_P,y_m,TTr_P] = P_tune(sys1_P,zeter,num0,den0)
%比例系数整定
   %构造传递函数
   [num_P,den_P]=GouZaotf(sys1_P,num0,den0);          %待整定传递函数系数
   %稳定性判断
   %调用WenDingXing函数，返回稳定性判断标志k,k=0--系统不稳定，k=1--系统稳定
   K_P=WenDingXing(num_P,den_P);  
   %调用DongTaiZhiBiao函数，返回上升时间Tr0、调节时间Ts0、衰减比sjb0
   [~,Ts_P,sjb_P,TTr_P,JingCha,y_m]=DongTaiZhiBiao(num_P,den_P);
   %判断衰减比是否为4:1 ?
   if sjb_P>=3 && sjb_P<=5 && K_P==1 && TTr_P~=0 && Ts_P~=0
        Zp=zeter;                                    %选定比例度，并留有一定裕量
   else Zp=0;                                        %不符合条件的Zp值
   end  
end


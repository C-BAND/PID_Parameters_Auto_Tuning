function [ Tr,Ts,sjb,TTr,JingCha,y_m] = DongTaiZhiBiao(num,den)
%Tr为系统的上升时间,Ts为系统的调节时间，通过比较Tr和Ts大小判断系统快速性
%sjb为系统衰减比，判断是否为4:1？
%求系统动态指标
   %p=input('采样时间=');                    %输入采样时间
   p=0.001;
   t=0:p:50;                                 %时间
   m=length(t);                              %采样点个数
   %负数域-->时域表达式
   [y,~,t]=step(num,den,t);
   y_m=y(m);                                 %响应终值
   JingCha=1-y_m;                            %计算静差
   r1=1;while y(r1)<0.1*y_m,r1=r1+1;end;
   r2=1;while y(r2)<0.9*y_m,r2=r2+1;
        if r2==2501 
        break;end
        end
   Tr=(r2-r1)*p;                             %上升时间
   [ymax,tp1]=max(y);      
   %peak_time=tp*p;                          %峰值时间
   
   s=m;while y(s)>0.98*y_m && y(s)<1.02*y_m;s=s-1;end
   Ts=s*p;                                   %调节时间
   max_overshoot_1=ymax-y_m;                 %第一个峰值-超调量
   
   %求第二个峰值
   T=find(abs(y-y_m)<0.001);                 %寻找输出值与稳态值相等的采样点
   %T=T*p;                                   %转换为仿真时间
   l_T=length(T);                            %求相等点的个数
   %调用fing_fun函数，寻找与稳态值相同的标号
   [TT(1),n1]=find_fun(T,1,l_T);             %第一组数据对应采样点TT(1)
   [TT(2),n2]=find_fun(T,n1+1,l_T);          %第二组数据对应采样点TT(2)
   [TT(3),n3]=find_fun(T,n1+n2+1,l_T);       %第三组数据对应采样点TT(3)
   [TT(4),n4]=find_fun(T,n1+n2+n3+1,l_T);    %第四组数据对应采样点TT(4)
   if TT(3)~=TT(4) && TT(3)~=0 && TT(4)~=0   %判断有无两个峰值
   [yx2,tp20]=max(y(TT(3):TT(4)));           %第二个峰值
   tp2=tp20+TT(3);                           %第二个峰值出现时间
   max_overshoot_2=yx2-y_m;                  %第二个峰值-超调量
   TTr=(tp2-tp1)*p;                          %两峰值间的时间差
   sjb=max_overshoot_1/max_overshoot_2;      %求衰减比
   else
       sjb=1000;
       TTr=0;                                %对没出现两个峰值的TTr=0
   end
end


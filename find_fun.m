function [ T,n ] = find_fun( Dai_f,k,l_T )
%找出矩阵中几个相同值，并返回它们的平均值
%T-每组数据平均值，n-每组数据长度,Dai_f-待寻值矩阵，k-寻值初值标号
if l_T>=4
m=find(abs(Dai_f-Dai_f(k))<1000); %返回与初值相近值标号
n=length(m);                      %m长度
T=Dai_f(k);                       %数据
else
    T=0;n=0;                      %不存在第二个峰值
end

